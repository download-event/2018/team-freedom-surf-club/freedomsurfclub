<?php

    include('../config.php');
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
        case "GET":
            // var_dump($_GET);
            $query = "SELECT se.id as idSegnalazione, date(se.data_ora) as data_ora, se.id_utente, st.id, st.testo as stato, count(va.id_segnalazione) as conteggio, ut.nome, so.testo, se.longitudine, se.latitudine FROM `segnalazioni` as se INNER JOIN sottocategorie as so on se.id_sottocategoria = so.id INNER JOIN uffici as uf on uf.id = so.id_ufficio INNER JOIN utenti as ut on ut.id = se.id_utente INNER JOIN stati as st on se.id_stato = st.id LEFT JOIN valutazioni as va on va.id_segnalazione = se.id";
            $query .= " WHERE st.id in (".($_GET["idStato"] != "" ? $_GET["idStato"] : "-1").")";
            $query .= " GROUP BY (se.id) ORDER BY date(se.data_ora), count(va.id_segnalazione) DESC";

            $result = mysqli_query($mysqli, $query);
            $rows = array();
            while($r = mysqli_fetch_assoc($result)) {
                $rows[] = $r;
            }
            print json_encode($rows);
            break;
        case "POST":
            break;
        case "PUT":
            parse_str(file_get_contents('php://input'), $_PUT);
            break;
        case "PATCH":
            parse_str(file_get_contents('php://input'), $_PATCH);
            
	        $result = true;
            $query = "UPDATE `segnalazioni` SET id_stato = ".$_PATCH["idStato"]." WHERE id = ".$_PATCH["idSegnalazione"];
            // $result = mysqli_query($mysqli, $query);
            if (!$mysqli->query($query)) {
                echo "ERRORE\nSi è verificato un errore inaspettato";
                unset($result);
            }
            if (isset($result)) {
                echo "YES";
             } 
            break;
        case "DELETE":
            break;
        default:
            echo "default";
            break;
    }

    // echo $method;
?>