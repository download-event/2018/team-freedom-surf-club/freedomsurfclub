import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapPage } from './map';

import { AddAlertPageModule } from '../add-alert/add-alert.module';

@NgModule({
  declarations: [
    MapPage,
  ],
  imports: [
    IonicPageModule.forChild(MapPage),
    AddAlertPageModule,
  ],
})
export class MapPageModule {}
