<?php

use Slim\Http\Request;
use Slim\Http\Response;

header("Access-Control-Allow-Origin: *");

// Routes
$app->get('/alert/all', function ($request, $response, $args) {
    
    $query = "SELECT s.id, ut.nome, ut.cognome, s.latitudine, s.longitudine, s.data_ora, s.id_stato, s.id_sottocategoria, h.testo, count(s.id) as conteggio FROM segnalazioni as s 
inner join sottocategorie as h on h.id = s.id_sottocategoria 
inner join utenti as ut on ut.id = s.id_utente 
left join valutazioni as v on v.id_segnalazione = s.id group by (s.id)";
    try {
        $statement = $this->db->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
         
         // If user exist
			if ($result) {
            
            $data=array();
            //$products_arr["records"]=array();
                
            foreach ($result as $a_alert){
                extract($a_alert);     
                $product_item=array(
                    "id" => utf8_encode($id),
                    "id_sottocategoria" => utf8_encode($id_sottocategoria),
                    "sottocategoria" => utf8_encode($testo),
                    
                    "utente" =>  utf8_encode($nome.' '.$cognome),
                    "dataora" => utf8_encode($data_ora),
                    "latitudine" => utf8_encode($latitudine),
                    "longitudine" => utf8_encode($longitudine),
                    "id_stato" => utf8_encode($id_stato),
                    "valutazione" => utf8_encode($conteggio)  );
                array_push($data, $product_item);
            }
             $response = $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json');
                
            $response =  $response->getBody()->write(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT));
                
			return $response;
            }

    } catch(PDOException $e) {
         
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write('{"error":{"text":'. $e->getMessage() .'}}');
    }
});

// Routes
$app->get('/alert/{id_utente}', function ($request, $response, $args) {
    
    $id_utente = $request->getAttribute("id_utente");
    
    $query = "SELECT s.id, ut.nome, ut.cognome, s.latitudine, s.longitudine, s.data_ora, s.id_stato, s.id_sottocategoria, h.testo, count(s.id) as conteggio FROM segnalazioni as s 
inner join sottocategorie as h on h.id = s.id_sottocategoria 
inner join utenti as ut on ut.id = s.id_utente 
left join valutazioni as v on v.id_segnalazione = s.id 
where s.id_utente = $id_utente
group by (s.id)";
    try {
        $statement = $this->db->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
         
         // If user exist
			if ($result) {
            
            $data=array();
            //$products_arr["records"]=array();
                
            foreach ($result as $a_alert){
                extract($a_alert);     
                $product_item=array(
                    "id" => utf8_encode($id),
                    "id_sottocategoria" => utf8_encode($id_sottocategoria),
                    "sottocategoria" => utf8_encode($testo),
                    
                    "utente" =>  utf8_encode($nome.' '.$cognome),
                    "dataora" => utf8_encode($data_ora),
                    "latitudine" => utf8_encode($latitudine),
                    "longitudine" => utf8_encode($longitudine),
                    "id_stato" => utf8_encode($id_stato)  );
                array_push($data, $product_item);
            }
             $response = $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json');
                
            $response =  $response->getBody()->write(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT));
                
			return $response;
            }

    } catch(PDOException $e) {
         
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write('{"error":{"text":'. $e->getMessage() .'}}');
    }
});

$app->get('/categorie/all', function ($request, $response, $args) {
    
    $query = "SELECT * FROM categorie";
     try {
        $statement = $this->db->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
         
         // If user exist
			if ($result) {
            
            $data=array();
            //$products_arr["records"]=array();
                
            foreach ($result as $a_alert){
                extract($a_alert);     
                $product_item=array(
                    "id" => utf8_encode($id),
                    "testo" => utf8_encode($testo)  );
                array_push($data, $product_item);
            }
             $response = $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json');
                
            $response =  $response->getBody()->write(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT));
                
			return $response;
            }

    } catch(PDOException $e) {
         
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write('{"error":{"text":'. $e->getMessage() .'}}');
    }
});

$app->get('/stati/all', function ($request, $response, $args) {
    
    $query = "SELECT * FROM stati";
     try {
        $statement = $this->db->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
         
         // If user exist
			if ($result) {
            
            $data=array();
            //$products_arr["records"]=array();
                
            foreach ($result as $a_alert){
                extract($a_alert);     
                $product_item=array(
                    "id" => utf8_encode($id),
                    "testo" => utf8_encode($testo)  );
                array_push($data, $product_item);
            }
             $response = $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json');
                
            $response =  $response->getBody()->write(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT));
                
			return $response;
            }

    } catch(PDOException $e) {
         
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write('{"error":{"text":'. $e->getMessage() .'}}');
    }
});


                
$app->get('/sottocategorie/{id_categorie}', function ($request, $response, $args) {
    try {
         // Gets username and password
		$id_categorie = $request->getAttribute("id_categorie");
        $query = "SELECT * FROM sottocategorie WHERE id_categorie = '$id_categorie';";
        $statement = $this->db->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
         
         // If user exist
			if ($result) {
            
            $data=array();
            //$products_arr["records"]=array();
                
            foreach ($result as $a_alert){
                extract($a_alert);     
                $product_item=array(
                    "id" => utf8_encode($id),
                    "testo" => utf8_encode($testo) );
                array_push($data, $product_item);
            }
             $response = $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json');
                
            $response =  $response->getBody()->write(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT));
                
			return $response;
            }

    } catch(PDOException $e) {
         
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write('{"error":{"text":'. $e->getMessage() .'}}');
    }
});

$app->post('/register/{nome}/{cognome}/{email}/{password}', function (Request $request, Response $response) {
		
		// Gets username and password
		$nome = $request->getAttribute("nome");
        $cognome = $request->getAttribute("cognome");
        $email = $request->getAttribute("email");
		$pass = $request->getAttribute("password");
    
    $query = "INSERT INTO utenti( nome, cognome, email, password) VALUES( '$nome', '$cognome', '$email', '$pass' )";
    try {
			$stmt = $this->db->prepare($query);
            $result = $stmt->execute();
        if($result)
{         $messaggio['esito']="Nuovo utente inserito con successo."; 
        
        return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode($messaggio));
 }else{
            $messaggio['esito']="Errore: Nuovo utente non inserito."; 
        
        return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode($messaggio));
        }

    } catch(PDOException $e) {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write('{"error":{"text":'. $e->getMessage() .'}}');
    }
});

$app->post('/alert/{id_sottocategoria}/{id_utente}/{latitudine}/{longitudine}', function (Request $request, Response $response) {
		
		// Gets username and password
		$id_sottocategoria = $request->getAttribute("id_sottocategoria");
        $id_utente = $request->getAttribute("id_utente");
		$latitudine = $request->getAttribute("latitudine");
    $longitudine = $request->getAttribute("longitudine");
    
    $query = "INSERT INTO segnalazioni( id_sottocategoria, id_utente, data_ora, latitudine, longitudine, id_stato) VALUES( $id_sottocategoria, $id_utente, NOW(), $latitudine, $longitudine, 1 )";
    try {
			$stmt = $this->db->prepare($query);
            $result = $stmt->execute();
        if($result)
        {       
         $query = "INSERT INTO valutazioni( id_seganazione, id_utente ) VALUES( $id_seganazione, $id_utente )";
        
        $stmt = $this->db->prepare($query);
        $result = $stmt->execute();
        $messaggio['esito']="Segnalazione inserita con successo."; 
        return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode($messaggio));
        }else{
            $messaggio['esito']="Errore: Segnalazione non inserita. $query"; 
        
        return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode($messaggio));
        }

    } catch(PDOException $e) {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write('{"error":{"text":'. $e->getMessage() .'}}');
    }
});

$app->post('/login/{email}/{password}', function (Request $request, Response $response) {
		// Gets username and password
		$email = $request->getAttribute("email");
		$pass = $request->getAttribute("password");

		try {
			// Gets the user into the database
			$query = "SELECT * FROM utenti WHERE email='$email';";
			$statement = $this->db->prepare($query);
            $statement->execute();
			$query = $statement->fetchObject();

			// If user exist
			if ($query) {
				// If password is correct
				if ($pass === $query->password) {
					$data['id'] = $query->id;
                    
                    $data['status'] = "ok, you are in.";
                    $data['nome'] = $query->nome;
                    $data['cognome'] = $query->cognome;
                    $data['id'] = $query->id;
                        
				} else {
					// Password wrong
					$data['status'] = "Error: The password you have entered is wrong. $email --- $pass";
				}
			} else {
				// Username wrong
				$data['status'] = "Error: The user specified does not exist.";
			}

			// Return the result
			$response = $response->withHeader('Content-Type', 'application/json');
			$response = $response->withStatus(201, 'Created');
			$response = $response->getBody()->write(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT));
			return $response;
		} catch(PDOException $e) {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write('{"error":{"text":'. $e->getMessage() .'}}');
       }
	});

$app->post('/valutazione/{id_segnalazione}/{id_utente}', function (Request $request, Response $response) {
		// Gets username and password
		$id_segnalazione = $request->getAttribute("id_segnalazione");
		$id_utente = $request->getAttribute("id_utente");

		try {
			// Gets the user into the database
			$query = "INSERT INTO valutazioni(id_utente, id_segnalazione) VALUES ($id_segnalazione, $id_utente);";
			$statement = $this->db->prepare($query);
            $statement->execute();
			$query = $statement->rowCount();

			// If user exist
			if ($query==1) {
					
                    $data['status'] = "Ok: Done.";
                    
				} else {
					
					$data['status'] = "Error: User has just voted for this alert.";
				}
			

			// Return the result
			$response = $response->withHeader('Content-Type', 'application/json');
			$response = $response->withStatus(201, 'Created');
			$response = $response->getBody()->write(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT));
			return $response;
		} catch(PDOException $e) {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write('{"error":{"text":'. $e->getMessage() .'}}');
       }
	});




