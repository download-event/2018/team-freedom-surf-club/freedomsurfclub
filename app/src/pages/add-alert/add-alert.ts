import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { SegnalazioniProvider } from '../../providers/segnalazioni/segnalazioni';

import { Geolocation } from '@ionic-native/geolocation';

@IonicPage()
@Component({
  selector: 'page-add-alert',
  templateUrl: 'add-alert.html',
})
export class AddAlertPage {

  category: number;
  mylat: number;
  mylng: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public geolocation: Geolocation, public alertCtrl: AlertController,
    public segnalazioniProvider: SegnalazioniProvider) {
    this.category = 0;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddAlertPage');
  }

  changeCategory(n: number) {
    this.category = n;
  }

  postAlert(subcategory) {
    this.getCurrentPosition().then(() => {
      this.segnalazioniProvider.postSegnalazione(1, subcategory, this.mylat, this.mylng).then(() => {
          let alert = this.alertCtrl.create({
            title: 'Segnalazione inviata',
            buttons: [{
              text: 'OK',
              handler: () => {
                this.navCtrl.pop();
              }
            }]
          });
          alert.present();
      }).catch(e => console.log(e));
    }).catch(e => console.log(e));
  }

  getCurrentPosition() {
    return new Promise(resolve => {
      this.geolocation.getCurrentPosition().then((position) => {
        this.mylat = position.coords.latitude;
        this.mylng = position.coords.longitude;
        resolve(true);
      }).catch((err) => {
        console.log(err);
      });
    });
  }
}
