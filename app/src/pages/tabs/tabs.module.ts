import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabsPage } from './tabs';

import { HomePageModule } from '../home/home.module';
import { MapPageModule } from '../map/map.module';
import { ListPageModule } from '../list/list.module';
import { MenuPageModule } from '../menu/menu.module';

@NgModule({
  declarations: [
    TabsPage,
  ],
  imports: [
    IonicPageModule.forChild(TabsPage),
    HomePageModule,
    MapPageModule,
    ListPageModule,
    MenuPageModule
  ],
})
export class TabsPageModule {}
