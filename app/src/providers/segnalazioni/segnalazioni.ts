import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

export class Segnalazione {
  id: number;
  latitudine: number;
  longitudine: number;
  id_sottocategoria: number;
  sottocategoria: string;
  id_categoria: number;
  categoria: string;
  dataora: Date;
  id_stato: number;
  id_utente: number;
  utente: string;
}

@Injectable()
export class SegnalazioniProvider {

  constructor(public http: HttpClient) {
    console.log('Hello SegnalazioniProvider Provider');
  }

  getSegnalazioni(): Promise<any> {
    let url = "http://www.freedomsurfclub.altervista.org/index.php/alert/all";
    return new Promise(resolve => {
      this.http.get(url).subscribe(data => {
        console.log("GET: " + data);
        resolve(data);
      }, e => {
        console.log(e);
      });
    });
  }

  getSegnalazioniUtente(id): Promise<any> {
    let url = "http://www.freedomsurfclub.altervista.org/index.php/alert/" + id;
    return new Promise(resolve => {
      this.http.get(url).subscribe(data => {
        console.log("GET: " + data);
        resolve(data);
      }, e => {
        console.log(e);
      });
    });
  }

  postSegnalazione(utente, sottocategoria, lat, lng) {
    let url = "http://www.freedomsurfclub.altervista.org/index.php/alert/";
    url += sottocategoria + "/";
    url += utente + "/";

    url += this.round(lat, 5) + "/";
    url += this.round(lng, 5) + "";

    url = encodeURI(url);

    return new Promise(resolve => {
      this.http.post(url, {}, {}).subscribe(data => {
        resolve(data);
      }, e => {
        console.log(e);
      });
    });
  }

  round(number: number, decimal) {
    let ten = 1;
    for(let i=0; i<decimal; i++)
      ten *= 10;
    number *= ten;
    //console.log(number);
    number = Math.trunc(number);
    //console.log(number);
    number /= ten;
    //console.log(number);
    return number;
  }

  removeDot(number: number) {
    let string: string;
    string = number.toString();
    //console.log(string);
    string = string.replace(".", ",");
    //console.log(string);
    return string;
  }

  appoggiaSegnalazione(id_segnalazione, id_utente) {
    let url = "http://www.freedomsurfclub.altervista.org/index.php/valutazione/";
    url += id_segnalazione + "/";
    url += id_utente;

    url = encodeURI(url);

    return new Promise(resolve => {
      this.http.post(url, {}, {}).subscribe(data => {
        resolve(data);
      }, e => {
        console.log(e);
      });
    });
  }
}
