<?php
	session_start();
	include_once("../config.php");
	
	if(!isset($_SESSION['username']) || empty($_SESSION['username']) || $_SESSION['usertype']==2) 
		header("location: ../");

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<title>Amministratore</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>  
    	<link rel="stylesheet" href="../css/style.css">

		<script>
			Date.prototype.toDateInputValue = (function() {
				var local = new Date(this);
				local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
				return local.toJSON().slice(0,10);
			});

			function chiamata() {
				console.log($('#datada').val(), $('#dataa').val());
				$.ajax({
					type: "GET",
					url: "../api/uffici.php?da="+$('#datada').val()+"&a="+$('#dataa').val(),
					success: function(data) {
						console.log(data);
						var a = JSON.parse(data);
						var b = new Array();
						var i = 0;
						a.forEach(function(data) {
							i++;
							if (i <= 3)
								b.push(data["unome"]);
						})

						console.log(a);

						var ctx = document.getElementById('myChart').getContext('2d');
						var chart = new Chart(ctx, {
							type: 'bar',
							data: {
								labels: b,
								datasets: [{
									label: "Richieste accettate",
									backgroundColor: 'rgb(255, 00, 00)',
									borderColor: 'rgb(255, 00, 00)',
									data: [a[0]["conteggio"], a[1]["conteggio"], a[2]["conteggio"]],
								}, {
									label: "Problemi in elaborazione",
									backgroundColor: 'rgb(255, 255, 024)',
									borderColor: 'rgb(255, 255, 024)',
									data: [a[3]["conteggio"], a[4]["conteggio"], a[5]["conteggio"]],
								}, {
									label: "Problemi risolti",
									backgroundColor: 'rgb(0, 160, 0)',
									borderColor: 'rgb(0, 160, 0)',
									data: [a[6]["conteggio"], a[7]["conteggio"], a[8]["conteggio"]],
								}]
							},
							options: {
								scales: {
									xAxes: [{
										stacked: true
									}],
									yAxes: [{
										stacked: true
									}]
								}
							}
						});
					}
				});
			}

			$(document).ready(function() { 
				var month = new Date();
				month.setMonth(month.getMonth() -1);
				$('#dataa').val(new Date().toDateInputValue());
				$('#datada').val(new Date(month).toDateInputValue());
				chiamata();

				$(".datona").change(function (data) {
					chiamata();
				});
			});
</script>
<style>
	</style>
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<div class="container">
				<div class="navbar-brand">
				<?php
					echo htmlentities($_SESSION["username"]);
				?>
				</div>
				<a href="../logout.php" class="btn btn-outline-danger my-2 my-sm-0" role="button">Logout</a>
			</div>
		</nav>
		<main role="main">
        <div class="jumbotron">
			<div class="container">
				<h1 class="display-4">
					Amministrazione
				</h1>
			</div>
        </div>
		<div class="container">
			<div class="row">
				<div class='col-sm-4'>
					<div class="form-group">
                		<label for="datada" class="form-label">Da</label>
						<input type='date' id='datada' class="datona form-control" />
					</div>
				</div>
				<div class='col-sm-2'>
				</div>
				<div class='col-sm-4'>
					<div class="form-group">
                		<label for="dataa" class="form-label">A</label>
						<input type='date' id='dataa' class="datona form-control" />
					</div>
				</div>
			</div>
			<canvas id="myChart"></canvas>
		</div>
		</main>
		<footer class="container">
			<hr>
			<p>&copy; Freedom Surf Club</p>
		</footer>
	</body>
</html>