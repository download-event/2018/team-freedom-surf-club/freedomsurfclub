import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { UtenteProvider } from '../../providers/utente/utente';
import { LoginPage } from '../login/login'

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  email: string;
  password2: string;
  password: string;
  surname: string;
  name: string;

  constructor(public navCtrl: NavController, public utenteProvider: UtenteProvider,
    public navParams: NavParams, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  signup() {
    let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let isEmailValid = emailRegex.test(this.email);
    let passwordRegex = /^[a-zA-Z\d]{8,20}$/;
    let isPasswordValid = passwordRegex.test(this.password);
    if(!isEmailValid)
    {
      let alert = this.alertCtrl.create({
        title: 'Email non valida',
        subTitle: "L'email inserita non è valida",
        buttons: ['Chiudi']
      });
      alert.present();
    }
    else if(!isPasswordValid)
    {
      let alert = this.alertCtrl.create({
        title: "Scegli un'altra password",
        subTitle: 'La password deve essere di almeno 8 caratteri contenere almeno una lettera maiuscola, una minuscola e un numero',
        buttons: ['Chiudi']
      });
      alert.present();
    }
    else if(this.password != this.password2)
    {
      let alert = this.alertCtrl.create({
        title: 'La password non corrispondo',
        subTitle: "Prova a reinserirle",
        buttons: ['Chiudi']
      });
      alert.present();
    }
    else
    {
      /*let loading = this.loadingCtrl.create({
        content: 'Attendere...'
      });
      loading.present();*/
      
      this.utenteProvider.signup(this.email, this.password, this.name, this.surname).then(allowed => { 
        if(allowed)
        {
          let alert = this.alertCtrl.create({
            title: 'Registrazione eseguita con successo',
            subTitle: //"A breve ti arriverà un'email per la conferma della registrazione",
            "Ora potrai effettuare il login",
            buttons: ['Chiudi']
          });
          alert.present();
          this.navCtrl.setRoot(LoginPage);
        } 
        else
        {
          let alert = this.alertCtrl.create({
            title: 'Registrazione fallita',
            subTitle: "L'email è già stata utilizzata",
            buttons: ['Riprova']
          });
          alert.present();
        }
      }).catch(e => console.log(e));
    }
  }

}
