<?php
	session_start();
	include_once("../config.php");
	if(!isset($_SESSION['username']) || empty($_SESSION['username']) || $_SESSION['usertype']==1) 
		header("location: ../");

	$query = 'SELECT u.id, u.nomi FROM impiegati AS i INNER JOIN uffici AS u ON i.id_ufficio = u.id WHERE i.id_dipendente="'.$_SESSION['id_dipendente'].'"';
	// echo $query;
	$result = mysqli_query($mysqli, $query);
	if(mysqli_num_rows($result)>0){  
		$row = mysqli_fetch_assoc($result);
		$_SESSION["id_ufficio"] = $row["id"];
		$_SESSION["ufficio"] = $row["nomi"];
		// echo var_dump($row);
	} else {
		header("location: ../");
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<title>Utente</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    	<link rel="stylesheet" href="../css/style.css">
		<script>
			function chiamata () {
				var sList = "";
				$('input[type=checkbox]').each(function () {
					var sThisVal = (this.checked ? $(this).val() : "");
					if (sThisVal != "") sList += (sList=="" ? sThisVal : "," + sThisVal);
				});

				$.ajax({
					type: "GET",
					url: "../api/segnalazioni.php?idStato="+sList+"&idUfficio=" + $('#idUfficio').val(),
					success: function(data) {
						var a = JSON.parse(data);
						var table = '<table class="table table-striped table-bordered text-center">';
						table += '<thead><th>ID</th><th>Segnalazione</th><th>Data</th><th>Indirizzo</th><th>Paese</th><th>Visualizza</th>'
						a.forEach(function(data) {
							$.ajax({
								type: "GET",
								url: "https://api.opencagedata.com/geocode/v1/json?q=" + data["latitudine"] + "+" + data["longitudine"] + "&key=96341b62ee5548e4bf90e3e927139101",
								async: false,
								success: function(add) {
									table += "<tr class=" + data["stato"] + " id=" + data["idSegnalazione"]+ ">";
									table += "<td>" + data["idSegnalazione"] + "</td>";
									table += "<td>" + data["testo"] + " (" + data["conteggio"] +") </td>";
									table += "<td>" + data["data_ora"] + "</td>";
									table += "<td>" + (add["results"][0]["components"]["road"] == undefined ? "" : add["results"][0]["components"]["road"]) + "</td>";
									table += "<td>" + add["results"][0]["components"]["village"] + "</td>";
									table += "<td style='cursor: pointer'>" + "<a style='margin-right: 16px' href=\"https://www.google.com/maps/?q=" + data["latitudine"]+ ", " + data["longitudine"]+ "\"><i class='material-icons back'>map</i></a>" + "<i class='material-icons back red'>close</i><i class='material-icons back warning' >priority_high</i><i class='material-icons back check' >check</i>" + "</td>";
									table += "</tr>";
								}
							})
							
						})
						table += "</table>";
						$( "#tabella" ).html(table);
					}
				});
			}

			function updateSegnalazione(id, idStato) {
				console.log(id, idStato);
				$.ajax({
					type: "PATCH",
					url: "../api/segnalazioni.php",
					data: {
						idStato: idStato,
						idSegnalazione: id
					},
					success: function(data) {
						if (data == "YES") {
							chiamata();
						} else {
							alert(data);
						}
					},
					error: function(data) {
						console.log(data);
					}
				});
			}

			$( document ).ready(function() {
				chiamata();

				$("#tabella").on("click",".red", function(){
					var a = $(this).parents("tr");
					console.log(a.attr("id"));
					updateSegnalazione(a.attr("id"), 1);
				});

				$("#tabella").on("click",".check", function(){
					if (confirm("La segnalazione è stata risolta completamente?")) {
						var a = $(this).parents("tr");
						console.log(a.attr("id"));
						updateSegnalazione(a.attr("id"), 3);
					}
				});

				$("#tabella").on("click",".warning", function(){
					var a = $(this).parents("tr");
					console.log(a.attr("id"));
					updateSegnalazione(a.attr("id"), 2);
				});

				
				$(".form-check-input").change(function() {
					chiamata();
				});
			});
		</script>

	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<div class="container">
				<div class="navbar-brand">
				<?php
					echo htmlentities($_SESSION["username"]);
				?>
				</div>
				<a href="../logout.php" class="btn btn-outline-danger my-2 my-sm-0" role="button">Logout</a>
			</div>
		</nav>
        <div class="jumbotron">
			<div class="container">
				<input type="hidden" id="idUfficio" value="<?php echo $_SESSION["id_ufficio"] ?>"/>
				<input type="hidden" id="idClasse" value="<?php echo $idClasse ?>"/>
				<h1 class="display-4">
				<?php
					echo htmlentities($_SESSION["ufficio"]);
				?>
				</h1>
			</div>
        </div>
		<div class="container">
			<div class="form-check form-check-inline">
				<input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="1" checked>
				<label class="form-check-label text-danger" for="inlineCheckbox1">Attesa</label>
			</div>
			<div class="form-check form-check-inline">
				<input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="2">
				<label class="form-check-label text-warning" for="inlineCheckbox2">Processo</label>
			</div>
			<div class="form-check form-check-inline">
				<input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="3">
				<label class="form-check-label text-success" for="inlineCheckbox3">Completato</label>
			</div>
			<div id="tabella" class="table-responsive" style="margin-top: 10px">
			</div>
			<div class="pagination">
		</div>
		<footer class="container">
			<hr>
			<p>&copy; Freedom Surf Club</p>
		</footer>
	</body>
</html>