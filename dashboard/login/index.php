<?php

	global $error;
	session_start();
	include_once("../config.php");
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$username = mysqli_real_escape_string($mysqli, $_POST["username"]);
		$password = mysqli_real_escape_string($mysqli, md5($_POST["password"]));
		$query = 'SELECT * FROM dipendenti WHERE email="'.$username.'" AND password="'.$password.'";';
		// echo $query;
		$result = mysqli_query($mysqli, $query);
		if(mysqli_num_rows($result)>0){  
			$row = mysqli_fetch_assoc($result);
			$_SESSION["username"] = $username;
			$_SESSION["id_dipendente"] = $row["id"];
			$_SESSION["usertype"] = $row["tipo"];
			echo $query;
			echo "<br>";
			if ($_SESSION["usertype"]==1)
				header("Location: ../admin/");
			else
				header("Location: ../user/");
		}
		else {
			// echo $query;
			$error = "Credenziali non valide";
		}
	}
?>
<!DOCTYPE html>
<html>
  <head>
		<title>Login</title>
		
		<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	<link rel="stylesheet" href="../css/style.css">
		
  </head>
	<body class="login-page">
		<div class="container">
        <form method='POST' class="form-signin">
						<h2 class="form-signin-heading">Accesso alla piattaforma</h2>
            <input type="text" name="username" class="form-control" placeholder="Email" required autofocus>
            <input type="password" name="password" class="form-control" placeholder="Password" required>
						<?php
							echo "<p class='error'>$error</p>";
						?>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Accedi</button>
				</form>
		</div>
		<footer class="container">
			<hr>
			<p>&copy; Freedom Surf Club</p>
		</footer>

	</body>
</html>
