<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Views\PhpRenderer;


if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}
require __DIR__ . '/vendor/autoload.php';

session_start();

$settings = require __DIR__ . '/src/settings.php';

// Instantiate the app
$app = new \Slim\App($settings);
//To get the container, we can add the following after the line 
//where we create $app and before we start to register the routes in our application
$container = $app->getContainer();

$container['renderer'] = new PhpRenderer('/templates/');

$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'],
        $db['user'], $db['pass']);
    return $pdo;
};

// Set up dependencies
require __DIR__ . '/src/dependencies.php';
// Register middleware
//require __DIR__ . '/../src/middleware.php';
// Register routes
require __DIR__ . '/src/routes.php';
// Run app
$app->run();