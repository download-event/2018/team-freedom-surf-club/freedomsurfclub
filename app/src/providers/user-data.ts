import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';


@Injectable()
export class UserData {

  constructor(
    public storage: Storage
  ) {}

  setUser(user: string): void {
    this.storage.set('user', JSON.stringify(user));
  };

  getUser(): Promise<string> {
    return this.storage.get('user').then((value) => {
      return JSON.parse(value);
    });
  };

  setNome(user: string): void {
    this.storage.set('nome', JSON.stringify(user));
  };

  getNome(): Promise<string> {
    return this.storage.get('nome').then((value) => {
      return JSON.parse(value);
    });
  };
}
