import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

// to generate hash like sha-512
import CryptoJS from 'crypto-js';

@Injectable()
export class UtenteProvider {

  constructor(public http: Http) {
    console.log('Hello UtenteProvider Provider');
  }

  login(email, password) {
    password = CryptoJS.MD5(password);
    password = password.toString(CryptoJS.enc.Hex);

    let url = "http://www.freedomsurfclub.altervista.org/index.php/login/";
    url += email + "/";
    url += password;

    url = encodeURI(url);

    return new Promise(resolve => {
      this.http.post(url, null).subscribe(data => {
        resolve(data);
      }, e => {
        console.log(e);
      });
    });
  }

  signup(email, password, cognome, nome) {
    password = CryptoJS.MD5(password);
    password = password.toString(CryptoJS.enc.Hex);

    let url = "http://www.freedomsurfclub.altervista.org/index.php/register/";
    url += nome + "/";
    url += cognome + "/";
    url += email + "/";
    url += password;

    url = encodeURI(url);

    return new Promise(resolve => {
      this.http.post(url, null).subscribe(data => {
        resolve(data);
      }, e => {
        console.log(e);
      });
    });
  }
}
